import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		videos: [
			{title: "Lifestyle", videos: [
				{title: "Mercury Poisoning Took Everything From Me", id:"t0V0tTPFrho"},
				{title: "My A1C and Diabetes Kept Getting Worse", id:"u-9nB9RZeSg"},
				{title: "Ana Joseph (a walking Miracle)", id:"OSuEXsy7xPA"},
				{title: "I Was Going to Die", id:"_S6nPtbTCdA"},
			]},
			{title: "Mental Health", videos: [
				{title: "I Was Microscopically Losing Blood", id:"8hmtW_Qrxyk"},
				{title: "I Was Going to Drive Myself Off a Bridge", id:"I_hYNFN1b6w"},
				{title: "How to Recover From a Mental Illness", id:"N7AtJg2TkuU"},
				{title: "I Have Rectal Cancer But I Have Peace", id:"6vqKpS-dMh4"},
			]},
			{title: "Others", videos: [
				{title: "This Program Helped Me Stabilize My Blood Pressure", id:"nxK6amfKvlw"},
				{title: "Veteran Fights Depression  Reverse Disease Naturally", id:"NTfDYf3YNmA"},
				{title: "I Broke the Weight Loss Record", id:"0rornEjrX0k"},
				{title: "Fight T2 Diabetes", id:"wFR1xCiyDec"},
			]},
		]
	}
});

export default app;